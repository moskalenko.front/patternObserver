document.addEventListener('DOMContentLoaded', function(){
    function Observer(){
        this.array = []

        this.subscribe = function (fn) {
            this.array.push(fn)
        }
        
        this.unsubscribe = function (fn) {
            this.array = this.array.filter(function(sub){
                return sub !== fn
            })
        }

        this.publish = function (data) {
            this.array.forEach(function(sub){
                sub(data);
            });
        }
    }

    (function output(global, window, document, undefined){
        var textareaField = document.querySelector('.input'),
            outputField = document.querySelector('.output'),
            countField = document.querySelector('.count span'),
            reverseField = document.querySelector('.reverse'),
            outputObserver = new Observer();
        
        outputObserver.subscribe(function(data){
            outputField.innerHTML = data;
        });
        outputObserver.subscribe(function(data){
            countField.innerHTML = data.length;
        });
        outputObserver.subscribe(function(data){
            reverseField.innerHTML = data.split("").reverse().join("");
        });

        textareaField.addEventListener('keyup', function(){
            outputObserver.publish(this.value);
        })
    })(this, window, window.document)
});
